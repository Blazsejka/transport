package com.example.CarGoWithCargo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.example.CarGoWithCargo.model.Client;
import com.example.CarGoWithCargo.service.ClientService;

@SpringBootApplication
public class CarGoWithCargoApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(new Class<?>[] {CarGoWithCargoApplication.class, cgwcConfig.class}, args);
		
		context.getBean("clientService",ClientService.class);
		
		
	}

}
