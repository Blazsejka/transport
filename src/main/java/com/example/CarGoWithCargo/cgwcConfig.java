package com.example.CarGoWithCargo;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = "com.example.CarGoWithCargo")
@EnableJpaRepositories(basePackages = "com.example.CarGoWithCargo")
@ComponentScan(basePackages = "com.example.CarGoWithCargo")
@Configuration
public class cgwcConfig {

}
