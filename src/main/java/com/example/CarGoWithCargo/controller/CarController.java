package com.example.CarGoWithCargo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.CarGoWithCargo.model.Car;
import com.example.CarGoWithCargo.model.CarType;
import com.example.CarGoWithCargo.model.LoginSession;
import com.example.CarGoWithCargo.model.Transporter;
import com.example.CarGoWithCargo.service.CarService;
import com.example.CarGoWithCargo.viewmodel.ClientRegistrationFormData;
import com.example.CarGoWithCargo.viewmodel.LoginFormData;
import com.example.CarGoWithCargo.viewmodel.ProfileFormData;
import com.example.CarGoWithCargo.viewmodel.AddCarFormData;

@Controller
public class CarController {
	
	@Autowired
	private CarService carService;
	
	@Autowired
	private LoginSession session;
	
	@Autowired
	private ClientController clientController;
	
	@RequestMapping(value = "changeToCarOptionsPage", method = RequestMethod.GET)
	public String changeToCarOptionsPage(Model model) {
		if(null!=session.getSessionId()) {
			Long refId = session.getSessionId();
			if (null != clientController.getTransporter(refId)) {
				Transporter currentTransporter = clientController.getTransporter(refId);
				model.addAttribute("sessionId", session.getSessionId());
				model.addAttribute("currentTransporter", currentTransporter);
				return "caroptions.html";
			} else {
				model.addAttribute("sessionId", session.getSessionId());
				return "main.html";
			}
		}
		LoginFormData loginFormData = new LoginFormData();
		model.addAttribute("loginFormData", loginFormData);
		return "login.html";
	}
	
	
	@RequestMapping(value = "changeToAddCarsPage", method = RequestMethod.GET)
	public String changeToAddCarsPage(Model model) {
		if(null!=session.getSessionId()) {
			Long refId = session.getSessionId();
			Transporter currentTransporter = clientController.getTransporter(refId);
			AddCarFormData addCarFormData = new AddCarFormData();
			model.addAttribute("sessionId", session.getSessionId());
			model.addAttribute("currentTransporter", currentTransporter);
			model.addAttribute("addCarFormData", addCarFormData);
			return "addcars.html";
		}
		LoginFormData loginFormData = new LoginFormData();
		model.addAttribute("loginFormData", loginFormData);
		return "login.html";
	}
	
	
	@RequestMapping(value = "changeToUpdateCarsPage", method = RequestMethod.GET)
	public String changeToUpdateCarsPage(Model model) {
		return "updatecars.html";
	}
	
	
	@RequestMapping(value = "changeToRemoveCarsPage", method = RequestMethod.GET)
	public String changeToRemoveCarsPage(Model model) {
		return "removecars.html";
	}
	
	
	@RequestMapping(value="addCar", method = RequestMethod.POST)
	public String addCar(@ModelAttribute("addCarFormData") @Valid AddCarFormData addCarFormData, Model model, RedirectAttributes redirectAttributes) {
		
		if (null!=session.getSessionId()) {
			Long refId = session.getSessionId();
			Transporter currentTransporter = clientController.getTransporter(refId);
			String cbrand = addCarFormData.getCarBrand();
			String cmodel = addCarFormData.getCarModel();
			String cplate = addCarFormData.getCarPlate();
			String cmanufacturing = addCarFormData.getCarManufacturingYear();
			Integer ctotal = addCarFormData.getCarTotalWeight();
			Integer cseats = addCarFormData.getCarSeats();
			String ctype = addCarFormData.getCarType();
			Car car = new Car();
			
			if (null != currentTransporter && null != cbrand && null != cmodel && null != cplate && null != cmanufacturing && null != ctotal && null != cseats && null != ctype) {
				
				car.setCarOwner(currentTransporter);
				car.setCarBrand(cbrand);
				car.setCarModel(cmodel);
				car.setCarPlate(cplate);
				car.setCarManufacturingYear(Integer.parseInt(cmanufacturing));
				car.setCarTotalWeight(ctotal);
				car.setCarSeats(cseats);
								
				if (ctype.equalsIgnoreCase("trailer")) {
					car.setCarType(CarType.TRAILER);
				} else if (ctype.equalsIgnoreCase("dumptruck")) {
					car.setCarType(CarType.DUMPTRUCK);
				} else if (ctype.equalsIgnoreCase("van")) {
					car.setCarType(CarType.VAN);
				} else if (ctype.equalsIgnoreCase("flatbedtruck")) {
					car.setCarType(CarType.FLATBEDTRUCK);
				} else if (ctype.equalsIgnoreCase("boxedtruck")) {
					car.setCarType(CarType.BOXEDTRUCK);
				} else if (ctype.equalsIgnoreCase("camion")) {
					car.setCarType(CarType.CAMION);
				} else if (ctype.equalsIgnoreCase("containercar")) {
					car.setCarType(CarType.CONTAINERCAR);
				} else if (ctype.equalsIgnoreCase("minibus")) {
					car.setCarType(CarType.MINIBUS);
				} else if (ctype.equalsIgnoreCase("bus")) {
					car.setCarType(CarType.BUS);
				}
				
			}
			
			carService.saveCar(car);
			System.out.println("Saving the car was succesful.");
			
//			ScriptEngineManager manager = new ScriptEngineManager();
//		    ScriptEngine engine = manager.getEngineByName("JavaScript");
//
//		    String script1 = (String)"function hello(name) {print ('Hello, ' + name);}";
//		    String script2 = (String)"function getValue(a,b) { if (a===\"Number\") return 1; else return b;}";
//		    engine.eval(script1);
//		    engine.eval(script2);
//
//		    Invocable inv = (Invocable) engine;
//
//		    inv.invokeFunction("hello", "Scripting!!" );

			model.addAttribute("sessionId", session.getSessionId());
			model.addAttribute("currentTransporter", currentTransporter);
			return "caroptions.html";
			
		}
		
		LoginFormData loginFormData = new LoginFormData();
		model.addAttribute("loginFormData", loginFormData);
		return "login.html";
	}
	
	
	
	
	
}
