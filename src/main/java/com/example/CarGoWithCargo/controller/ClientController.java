package com.example.CarGoWithCargo.controller;

import java.time.LocalDate;
import com.example.CarGoWithCargo.viewmodel.ProfileFormData;
import java.time.format.DateTimeFormatter;

import javax.validation.Valid;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.CarGoWithCargo.model.Client;
import com.example.CarGoWithCargo.model.LoginSession;
import com.example.CarGoWithCargo.model.Transporter;
import com.example.CarGoWithCargo.service.ClientService;
import com.example.CarGoWithCargo.service.InvalidLoginException;
import com.example.CarGoWithCargo.service.TransporterService;
import com.example.CarGoWithCargo.viewmodel.ClientRegistrationFormData;
import com.example.CarGoWithCargo.viewmodel.LoginFormData;
import com.example.CarGoWithCargo.viewmodel.PasswordFormData;
import com.example.CarGoWithCargo.viewmodel.TransporterRegistrationFormData;

@Controller
public class ClientController {
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private TransporterService transporterService;
	
	@Autowired
	private LoginSession session;
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(Model model) {
		LoginFormData loginFormData = new LoginFormData();
		model.addAttribute("loginFormData", loginFormData);
		return "login.html";
	}
	
	
	@RequestMapping(value = "changeToClientRegistration", method = RequestMethod.GET)
	public String changeToClientRegistrationPage(Model model) {
		ClientRegistrationFormData crfd = new ClientRegistrationFormData();
		model.addAttribute("crfd", crfd);
		return "clientregistration.html";
	}
	
	
	@RequestMapping(value = "changeToTransporterRegistration", method = RequestMethod.GET)
	public String changeToTransporterRegistrationPage(Model model) {
		TransporterRegistrationFormData trfd = new TransporterRegistrationFormData();
		model.addAttribute("trfd", trfd);
		return "transporterregistration.html";
	}
		
	
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String submitLogin(@ModelAttribute("loginFormData") @Valid LoginFormData loginFormData, BindingResult bindingResult, Model model) {
		Transporter loginTransporter=null;
		Client loginClient=null;
		String checkEmail = loginFormData.getEmail();
		String checkPassword = loginFormData.getPassword();
		
		try {	
			loginTransporter = transporterService.login(checkEmail, checkPassword);
			System.out.println(loginTransporter);
			if (!(loginTransporter.equals(null))) {
				session.setSessionId(loginTransporter.getClientId());
				model.addAttribute("sessionId", session.getSessionId());
				return "main.html";
			}
		} catch (InvalidLoginException e) {
			System.out.println(e);
		}
		
		try {
			loginClient = clientService.login(checkEmail, checkPassword);
			if (!(loginClient.equals(null))) {
				session.setSessionId(loginClient.getClientId());
				model.addAttribute("sessionId", session.getSessionId());
				return "main.html";
			}
		} catch (InvalidLoginException e) {
			System.out.println(e);
		}
		
		model.addAttribute("loginFormData", loginFormData);
		return "login.html";
	}
	
	
	@RequestMapping(value = "clientRegistration", method = RequestMethod.POST)
	public String clientRegistration(@ModelAttribute("crfd") @Valid ClientRegistrationFormData clientRegistrationFormData, BindingResult bindingResult, Model model) {
		
		String regName = clientRegistrationFormData.getName();
		String regEmail = clientRegistrationFormData.getEmail();
		String regpassword1 = clientRegistrationFormData.getPassword1();
		String regpassword2 = clientRegistrationFormData.getPassword2();
		String regPlace = clientRegistrationFormData.getPlaceOfBirth();
		String tmpRegDate = clientRegistrationFormData.getDateOfBirth();
		String regPhone = clientRegistrationFormData.getPhoneNumber();
		
		System.out.println(regName);
		System.out.println(regEmail);
		System.out.println(regpassword1);
		System.out.println(regpassword2);
		System.out.println(regPlace);
		System.out.println(tmpRegDate);
		System.out.println(regPhone);
		
		LocalDate regDate=null;
		if (null!=tmpRegDate) {
			regDate = LocalDate.parse(tmpRegDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		}
		
		if (null!=regName && null!=regEmail && null!=regpassword1 && null!=regpassword2 && null!=regPlace && null!=regDate && null!=regPhone && regpassword1.equals(regpassword2)) {
			clientService.saveFullClient(regName, regPlace, regDate, regEmail, regPhone, regpassword1);
			LoginFormData loginFormData = new LoginFormData();
			model.addAttribute("loginFormData", loginFormData);
			return "login.html";
		}
		ClientRegistrationFormData crfd = new ClientRegistrationFormData();
		model.addAttribute("crfd", crfd);
		return "clientregistration.html";
	}
	
	
	@RequestMapping(value = "transporterRegistration", method = RequestMethod.POST)
	public String transporterRegistration(@ModelAttribute("trfd") @Valid TransporterRegistrationFormData transporterRegistrationFormData, BindingResult bindingResult, Model model) {
		
		String regName = transporterRegistrationFormData.getName();
		String regEmail = transporterRegistrationFormData.getEmail();
		String regpassword1 = transporterRegistrationFormData.getPassword1();
		String regpassword2 = transporterRegistrationFormData.getPassword2();
		String regPlace = transporterRegistrationFormData.getPlaceOfBirth();
		String tmpRegDate = transporterRegistrationFormData.getDateOfBirth();
		String regPhone = transporterRegistrationFormData.getPhoneNumber();
		String regCompany = transporterRegistrationFormData.getCompanyName();
		String regTax = transporterRegistrationFormData.getTaxNumber();
		
		System.out.println(regName);
		System.out.println(regEmail);
		System.out.println(regpassword1);
		System.out.println(regpassword2);
		System.out.println(regPlace);
		System.out.println(tmpRegDate);
		System.out.println(regPhone);
		System.out.println(regCompany);
		System.out.println(regTax);
		
		LocalDate regDate=null;
		if (null!=tmpRegDate) {
			regDate = LocalDate.parse(transporterRegistrationFormData.getDateOfBirth(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		}
		
		if (null!=regName && null!=regEmail && null!=regpassword1 && null!=regpassword2 && null!=regPlace && null!=regDate && null!=regPhone && regpassword1.equals(regpassword2) && null!=regCompany && null!= regTax) {
			transporterService.saveFullTransporter(regName, regPlace, regDate, regEmail, regPhone, regpassword1, regCompany, regTax);
			LoginFormData loginFormData = new LoginFormData();
			model.addAttribute("loginFormData", loginFormData);
			return "login.html";
		}
		TransporterRegistrationFormData trfd = new TransporterRegistrationFormData();
		model.addAttribute("trfd", trfd);
		return "transporterregistration.html";
	}

	
	public Transporter getTransporter(Long id) {	
		Transporter t = transporterService.getTransporterById(id);
		System.out.println("Megvan-e a transporter: "+t);
		if (null!=t) {
			return t;
		} else {
			return null;
		}
	}
	
	
	public Client getClient(Long id) {	
		Client c = clientService.getClientById(id);
		System.out.println("Megvan-e a client: "+c);
		if (null!=c) {
			return c;
		} else {
			return null;
		}
	}
	
	
	@RequestMapping(value = "changeToProfile", method = RequestMethod.GET)
	public String changeToProfilePage(Model model, RedirectAttributes redirectAttributes) {
		if (null != session.getSessionId()) {
			ProfileFormData profileFormData = new ProfileFormData();
			Long referenceId = session.getSessionId();
			Client pClient;
			Transporter pTrans;
			
			if (null!=getTransporter(referenceId)) {
				pTrans=getTransporter(referenceId);
				profileFormData.setCompanyName(pTrans.getTransporterCompanyName());
				profileFormData.setTaxNumber(pTrans.getTransporterTaxNumber());
				LocalDate ld = pTrans.getClientDateOfBirth();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				String referenceDate = ld.format(formatter);
				profileFormData.setDateOfBirth(referenceDate);
				profileFormData.setPlaceOfBirth(pTrans.getClientPlaceOfBirth());
				profileFormData.setEmail(pTrans.getClientEmail());
				profileFormData.setName(pTrans.getClientName());
				profileFormData.setPhoneNumber(pTrans.getClientPhoneNumber());
			} else {
				pClient=getClient(referenceId);
				LocalDate ld = pClient.getClientDateOfBirth();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				String referenceDate = ld.format(formatter);
				profileFormData.setDateOfBirth(referenceDate);
				profileFormData.setPlaceOfBirth(pClient.getClientPlaceOfBirth());
				profileFormData.setEmail(pClient.getClientEmail());
				profileFormData.setName(pClient.getClientName());
				profileFormData.setPhoneNumber(pClient.getClientPhoneNumber());
			}
			model.addAttribute("profileFormData", profileFormData);
			model.addAttribute("sessionId", session.getSessionId());
			return "profile.html";			
		} else {
			return session.handleInvalidLogin("login", redirectAttributes);
		}
		 
	}
	
	
	@RequestMapping(value = "updateProfile", method = RequestMethod.POST)
	public String updateProfile(@ModelAttribute("profileFormData") @Valid ProfileFormData profileFormData, Model model, RedirectAttributes redirectAttributes) {
		if (null != session.getSessionId()) {
			Long referenceId = session.getSessionId();
			Client pClient;
			Transporter pTrans;
			
			if (null!=getTransporter(referenceId)) {
				pTrans=getTransporter(referenceId);
				String regName = profileFormData.getName();
				String regEmail = profileFormData.getEmail();
				String regPlace = profileFormData.getPlaceOfBirth();
				String tmpRegDate = profileFormData.getDateOfBirth();
				String regPhone = profileFormData.getPhoneNumber();
				String regCompany = profileFormData.getCompanyName();
				String regTax = profileFormData.getTaxNumber();
				LocalDate regDate=null;				
				if (null!=tmpRegDate) {
					regDate = LocalDate.parse(tmpRegDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
				}
				
				if (null!=regName && null!=regEmail && null!=regPlace && null!=regDate && null!=regPhone) {
					pTrans.setClientName(regName);
					pTrans.setClientEmail(regEmail);
					pTrans.setClientPlaceOfBirth(regPlace);
					pTrans.setClientDateOfBirth(regDate);
					pTrans.setClientPhoneNumber(regPhone);
					if (null!=regCompany) {
						pTrans.setTransporterCompanyName(regCompany);
					} else {
						String company = pTrans.getTransporterCompanyName();
						pTrans.setTransporterCompanyName(company);
					}
					if (null!=regTax) {
						pTrans.setTransporterTaxNumber(regTax);
					} else {
						String tax = pTrans.getTransporterTaxNumber();
						pTrans.setTransporterTaxNumber(tax);
					}
					transporterService.saveTransporter(pTrans);
					session.setSessionId(pTrans.getClientId());
					model.addAttribute("sessionId", session.getSessionId());
					return "main.html";
				}
			}
				
			if (null!=getClient(referenceId)) {
				pClient=getClient(referenceId);
				String regName = profileFormData.getName();
				String regEmail = profileFormData.getEmail();
				String regPlace = profileFormData.getPlaceOfBirth();
				String tmpRegDate = profileFormData.getDateOfBirth();
				String regPhone = profileFormData.getPhoneNumber();
				LocalDate regDate=null;
				if (null!=tmpRegDate) {
					regDate = LocalDate.parse(tmpRegDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
				}
				
				if (null!=regName && null!=regEmail && null!=regPlace && null!=regDate && null!=regPhone) {
					pClient.setClientName(regName);
					pClient.setClientEmail(regEmail);
					pClient.setClientPlaceOfBirth(regPlace);
					pClient.setClientDateOfBirth(regDate);
					pClient.setClientPhoneNumber(regPhone);
					clientService.saveClient(pClient);
					session.setSessionId(pClient.getClientId());
					model.addAttribute("sessionId", session.getSessionId());
					return "main.html";
				}
			}
			
		}
			LoginFormData loginFormData = new LoginFormData();
			model.addAttribute("loginFormData", loginFormData);
			System.out.println("NEM FUTOTT LE AZ UPDATE :( ");
			return "login.html";	
	}
	
	
	@RequestMapping(value = "changeToPasswordChange", method = RequestMethod.GET)
	public String changeToPassword(Model model, RedirectAttributes redirectAttributes) {
		if (null != session.getSessionId()) {
			PasswordFormData passwordFormData = new PasswordFormData();
			model.addAttribute("passwordFormData", passwordFormData);
			model.addAttribute("sessionId", session.getSessionId());
			return "password.html";			
		} else {
			LoginFormData loginFormData = new LoginFormData();
			model.addAttribute("loginFormData", loginFormData);
			return "login.html";
		}
		 
	}
	
	
	@RequestMapping(value = "updatePassword", method = RequestMethod.POST)
	public String updatePassword(@ModelAttribute("passwordFormData") @Valid PasswordFormData passwordFormData, Model model, RedirectAttributes redirectAttributes) {
		if (null != session.getSessionId()) {
			Long referenceId = session.getSessionId();
			Client pClient;
			Transporter pTrans;
			String encryptedPassword = null;
			
			if (null!=getTransporter(referenceId)) {
				pTrans=getTransporter(referenceId);
				encryptedPassword=DigestUtils.sha1Hex(passwordFormData.getOldPassword());
				if (encryptedPassword.equals(pTrans.getClientPassword()) && passwordFormData.getNewPassword1().equals(passwordFormData.getNewPassword2())) {
					String newPass = DigestUtils.sha1Hex(passwordFormData.getNewPassword1());
					pTrans.setClientPassword(newPass);
					transporterService.saveTransporter(pTrans);
				}
				session.setSessionId(pTrans.getClientId());
				model.addAttribute("sessionId", session.getSessionId());
				return "main.html";
			}
		
			if (null!=getClient(referenceId)) {
				pClient=getClient(referenceId);
				encryptedPassword=DigestUtils.sha1Hex(passwordFormData.getOldPassword());
				if (encryptedPassword.equals(pClient.getClientPassword()) && passwordFormData.getNewPassword1().equals(passwordFormData.getNewPassword2())) {
					String newPass = DigestUtils.sha1Hex(passwordFormData.getNewPassword1());
					pClient.setClientPassword(newPass);
					clientService.saveClient(pClient);
				}
				session.setSessionId(pClient.getClientId());
				model.addAttribute("sessionId", session.getSessionId());
				return "main.html";
			}
		}	
		LoginFormData loginFormData = new LoginFormData();
		model.addAttribute("loginFormData", loginFormData);
		return "login.html";
	}
	
	
	
	
	
}
