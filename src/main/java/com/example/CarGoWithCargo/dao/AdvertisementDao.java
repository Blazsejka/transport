package com.example.CarGoWithCargo.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.CarGoWithCargo.model.Advertisement;
import com.example.CarGoWithCargo.model.Transporter;

@Repository
public interface AdvertisementDao extends JpaRepository<Advertisement, Long> {
	
	public List<Advertisement> findAll();
	
	public List<Advertisement> findAllByAdvertisementSource(Transporter source);
	
	public List<Advertisement> findAllByAdvertisementId(Long id);
	
	public List<Advertisement> findAllByAdvertisementTimeStamp(Date timeStamp);
	
	
}
