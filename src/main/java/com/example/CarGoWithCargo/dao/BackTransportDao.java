package com.example.CarGoWithCargo.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.CarGoWithCargo.model.BackTransport;
import com.example.CarGoWithCargo.model.Client;
import com.example.CarGoWithCargo.model.Transporter;

@Repository
public interface BackTransportDao extends JpaRepository<BackTransport, Long> {
	
	public List<BackTransport> findAll();
	
	public BackTransport findByBtId(Long id);
	
	public List<BackTransport> findByBtTransporter(Transporter transporter);
	
	public List<BackTransport> findByBtClient(Client client);
	
	public List<BackTransport> findByBtStart(Date start);
	
	

}
