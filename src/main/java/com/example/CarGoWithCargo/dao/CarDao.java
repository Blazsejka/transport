package com.example.CarGoWithCargo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.CarGoWithCargo.model.Car;
import com.example.CarGoWithCargo.model.CarType;
import com.example.CarGoWithCargo.model.Transporter;

@Repository
public interface CarDao extends JpaRepository<Car, Long> {

	public List<Car> findAll();
	
	public List<Car> findAllByCarType(CarType carType);
	
	public List<Car> findAllByCarOwner(Transporter owner);
	
	public Car findByCarId(Long id);
	
	public List<Car> findAllByCarBrand(String brand);
	
	public List<Car> findAllByCarModel(String model);
	
	public List<Car> findAllByCarTotalWeightGreaterThanEqual(Integer weight);
	
	public List<Car> findAllByCarTotalWeight(Integer weight);
	
	public List<Car> findAllByCarSeats(Integer seats);
	
	public Car findByCarPlate(String carPlate);
	
	public List<Car> findByCarFreeNowTrue();
	
	
	
	
}
