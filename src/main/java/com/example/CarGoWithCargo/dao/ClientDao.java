package com.example.CarGoWithCargo.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.CarGoWithCargo.model.Client;

@Repository
public interface ClientDao extends JpaRepository<Client, Long> {
	
	public List<Client> findAll();
	
	public Optional<Client> findByClientEmail(String clientEmail);	
	
	public Optional<Client> findByClientName(String clientName);	

	public List<Client> findByClientNameContaining(@Param("name") String clientNamePart);

	public Client findByClientId(@Param("id") Long clientId);

	@Query(value = "select c from Client c where c.clientEmail = :email")
	Client findByEmailAddress(@Param("email") String emailAddress);
	
	
}
