package com.example.CarGoWithCargo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.CarGoWithCargo.model.Client;
import com.example.CarGoWithCargo.model.Message;

@Repository
public interface MessageDao extends JpaRepository<Message, Long> {
	
	public List<Message> findAll();
	
	public List<Message> findByMessageToOrderByMessageTimeStampDesc(Client messageTo);
	
	public List<Message> findByMessageFrom(Client messageFrom);
	
	
}
