package com.example.CarGoWithCargo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.CarGoWithCargo.model.Client;
import com.example.CarGoWithCargo.model.Opinion;

@Repository
public interface OpinionDao extends JpaRepository<Opinion, Long> {
	
	public List<Opinion> findAll();
	
	public List<Opinion> findAllByOpinionTarget(Client target);
	
	public List<Opinion> findAllByOpinionSource(Client source);
	
	public List<Opinion> findByOpinionId(Long id);
	
	public List<Opinion> findAllByOpinionTargetOrderByOpinionTimeStampDesc(Client target);
	
	
}
