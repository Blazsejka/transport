package com.example.CarGoWithCargo.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.CarGoWithCargo.model.BackTransport;
import com.example.CarGoWithCargo.model.Client;
import com.example.CarGoWithCargo.model.Transport;
import com.example.CarGoWithCargo.model.Transporter;

@Repository
public interface TransportDao extends JpaRepository<Transport, Long> {
	
	public List<Transport> findAll();
	
	public Transport findByTId(Long id);
	
	public List<Transport> findByTTransporter(Transporter transporter);
	
	public List<Transport> findByTClient(Client client);
	
	public List<Transport> findByTStart(Date start);

	
	
}
