package com.example.CarGoWithCargo.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.CarGoWithCargo.model.Transporter;

@Repository
public interface TransporterDao extends JpaRepository<Transporter, Long> {
	
	public List<Transporter> findAll();
	
	public Optional<Transporter> findByClientEmail(String clientEmail);	
	
	public Optional<Transporter> findByClientName(String clientName);	

	public List<Transporter> findByClientNameContaining(@Param("name") String clientNamePart);

	public Transporter findByClientId(@Param("id") Long clientId);
	
	public Transporter findByTransporterTaxNumber(Long transporterTaxNumber);
	
	public Transporter findByTransporterCompanyName(String transporterCompanyName);
	
	@Query(value = "select t from Transporter t where t.clientEmail LIKE %:email%")
	Transporter findByEmailAddress(@Param("email") String emailAddress);
	
	@Query(value = "select t from Transporter t where t.transporterCompanyName LIKE %:companyName%")
	Transporter findByCompanyName(@Param("companyName") String companyName);
	
	@Query(value = "SELECT t FROM Transporter t JOIN t.cars c ON c.carPlate = :carPlate")
	Transporter findByCarPlate(@Param("carPlate") String carPlate);
	
	

}
