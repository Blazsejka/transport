package com.example.CarGoWithCargo.model;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name = "Advertisement")
public class Advertisement {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long advertisementId;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Transporter advertisementSource;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private java.util.Date advertisementTimeStamp;
	
	@Column(length=10485760, nullable = false)
	private String advertisementBody;

	
}
