package com.example.CarGoWithCargo.model;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name = "Back_Transport")
public class BackTransport {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long btId;
	
	@Column(nullable = false)
	private String btFrom;
	
	@Column(nullable = false)
	private String btTo;
	
	@Temporal(TemporalType.TIME)
	@Column(nullable = false)
	private java.util.Date btStart;
	
	@Temporal(TemporalType.TIME)
	@Column(nullable = false)
	private java.util.Date btEnd;
	
	@Column(nullable = false)
	private Integer btPricePerKm;
	
	@Column(nullable = false)
	private Long btPrice;
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Client btClient;
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Transporter btTransporter;
	
}
