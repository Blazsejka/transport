package com.example.CarGoWithCargo.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "Truck")
public class Car {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long carId;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Transporter carOwner;
	
	@Column(nullable = false)
	private CarType carType;
	
	@Column(nullable = false)
	private Integer carManufacturingYear;
	
	@Column(nullable = false)
	private String carBrand;
	
	@Column(nullable = false)
	private String carModel;
	
	@Column(nullable = false)
	private Integer carTotalWeight;
	
	@Column(nullable = false)
	private Integer carSeats;
	
	@Column(nullable = false)
	private String carPlate;
	
	private Boolean carFreeNow;
	
}
