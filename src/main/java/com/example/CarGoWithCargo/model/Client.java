package com.example.CarGoWithCargo.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "Client")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Client {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long clientId;
	
	@Column(nullable = false)
	private String clientName;
	
	@Column(nullable = false)
	private String clientPlaceOfBirth;
	
	@Column(nullable = false)
	private LocalDate clientDateOfBirth;
	
	@Email
	@Column(nullable = false)
	private String clientEmail;
	
	@Column(nullable = false)
	private String clientPhoneNumber;
	
	@Column(nullable = false)
	private String clientPassword;
	
}
