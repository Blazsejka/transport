package com.example.CarGoWithCargo.model;

import java.io.Serializable;
import com.example.CarGoWithCargo.viewmodel.LoginFormData;

import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Service
@ToString
@SessionScope
@AllArgsConstructor
@NoArgsConstructor
public class LoginSession implements Serializable {
	
	private Long sessionId;
	
	public String handleInvalidLogin(String pageRedirecting, RedirectAttributes redirectAttributes) {
		LoginFormData loginFormData = new LoginFormData();
		redirectAttributes.addFlashAttribute("loginFormData", loginFormData);
		return "login.html";
	}
	
}
