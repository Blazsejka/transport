package com.example.CarGoWithCargo.model;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name = "Message")
public class Message {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long messageId;
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Client messageFrom;
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Client messageTo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private java.util.Date messageTimeStamp;
	
	@Column(length=10485760, nullable = false)
	private String messageBody;
	
	
	
}
