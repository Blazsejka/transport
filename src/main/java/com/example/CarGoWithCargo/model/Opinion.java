package com.example.CarGoWithCargo.model;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name = "Opinion")
public class Opinion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long opinionId;
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Client opinionSource;
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Client opinionTarget;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private java.util.Date opinionTimeStamp;
	
	@Column(length=10485760, nullable = false)
	private String opinionBody;

	
	
}
