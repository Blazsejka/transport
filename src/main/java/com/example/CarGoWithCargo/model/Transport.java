package com.example.CarGoWithCargo.model;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name = "Transport")
public class Transport {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long tId;
	
	@Column(nullable = false)
	private String tFrom;
	
	@Column(nullable = false)
	private String tTo;
	
	@Temporal(TemporalType.TIME)
	@Column(nullable = false)
	private java.util.Date tStart;
	
	@Temporal(TemporalType.TIME)
	@Column(nullable = false)
	private java.util.Date tEnd;
	
	@Column(nullable = false)
	private Integer tPricePerKm;
	
	@Column(nullable = false)
	private Long tPrice;
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Client tClient;
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Transporter tTransporter;
	
	
	
}
