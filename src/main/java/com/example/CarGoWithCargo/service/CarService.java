package com.example.CarGoWithCargo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.CarGoWithCargo.dao.CarDao;
import com.example.CarGoWithCargo.model.Car;
import com.example.CarGoWithCargo.model.CarType;
import com.example.CarGoWithCargo.model.Transporter;

@Service
public class CarService {
	
	@Autowired
	private CarDao carDao;
	
	
	public List<Car> getAllCars() {
		return carDao.findAll();
	}
	
	
	public void saveCar(Car car) {
		if (null!=car) {
			carDao.saveAndFlush(car);
		}
	}
	
	
	public void saveFullCar(Transporter transporter, CarType ct, Integer year, String brand, String model, Integer totalWeight, Integer seats, String plate) {
		Car car = new Car();
		
		if (null!=transporter) {
			car.setCarOwner(transporter);
		}
		
		if (null!=ct) {
			car.setCarType(ct);
		}
		
		if (null!=year) {
			car.setCarManufacturingYear(year);
		}
		
		if (null!=brand) {
			car.setCarBrand(brand);
		}
		
		if (null!=model) {
			car.setCarModel(model);
		}
		
		if (null!=totalWeight) {
			car.setCarTotalWeight(totalWeight);
		}
		
		if (null!=seats) {
			car.setCarSeats(seats);
		}
		
		if (null!=plate) {
			car.setCarPlate(plate);
		}
		
		if (null!=car.getCarBrand() && null!=car.getCarManufacturingYear() && null!=car.getCarModel() && null!=car.getCarOwner() && null!=car.getCarPlate() && null!=car.getCarSeats() && null!=car.getCarTotalWeight() && null!=car.getCarType()) {
			carDao.saveAndFlush(car);
		}
	}
	
	
	public List<Car> getAllCarByOwner(Transporter transporter) {
		return carDao.findAllByCarOwner(transporter);
	}
	
	
	public List<Car> getAllCarByType(CarType ct) {
		return carDao.findAllByCarType(ct);
	}
	
	
	public List<Car> getAllCarBySeats(Integer seats) {
		return carDao.findAllByCarSeats(seats);
	}
	
	
	public Car getCarByPlate(String plate) {
		return carDao.findByCarPlate(plate);
	}
	
	
	public List<Car> getAllCarByMinimumTotalWeight(Integer weight) {
		return carDao.findAllByCarTotalWeightGreaterThanEqual(weight);
	}
	
	
	public List<Car> getAllCarByExactTotalWeight(Integer weight) {
		return carDao.findAllByCarTotalWeight(weight);
	}
	
	
	
	
	
}
