package com.example.CarGoWithCargo.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.codec.digest.DigestUtils;

import com.example.CarGoWithCargo.dao.ClientDao;
import com.example.CarGoWithCargo.model.Client;

@Service
public class ClientService {
	
	@Autowired
	private ClientDao clientDao;
	
	public List<Client> getAllClient() {
		return clientDao.findAll();
	}
	
	public void saveClient(Client c) {
		if (!(c.equals(null)))
		clientDao.saveAndFlush(c);
	}
	
	public void saveFullClient(String name, String place, LocalDate date, String email, String phonenumber, String password) {
		Client c = new Client();
		if (null!=name) {
			c.setClientName(name);
		}
		if (null!=place) {
			c.setClientPlaceOfBirth(place);
		}
		if (null!=date) {
			c.setClientDateOfBirth(date);
		}
		if (null!=email) {
			c.setClientEmail(email);
		}
		if (null!=phonenumber) {
			c.setClientPhoneNumber(phonenumber);
		}
		if (null!=password) {
			String encryptedPassword = DigestUtils.sha1Hex(password);
			c.setClientPassword(encryptedPassword);
		}
		if (null!=c.getClientName() && null!=c.getClientDateOfBirth() && null!=c.getClientEmail() && null!=c.getClientPassword() && null!=c.getClientPhoneNumber() && null!=c.getClientPlaceOfBirth()) {
			clientDao.saveAndFlush(c);
		}
	}
	
	
	public Client getClientByName(String name) {
		Optional<Client> validClient = clientDao.findByClientName(name);
		if (validClient.isPresent()) {
			Client c = validClient.get();
			return c;
		} else return null;
	}
	
	
	public Client getClientByEmail(String email) {
		Optional<Client> validClient = clientDao.findByClientEmail(email);
		if (validClient.isPresent()) {
			Client c = validClient.get();
			return c;
		} else return null;
	}
	
	
	public Client getClientById(Long id) {
		Optional<Client> validClient = clientDao.findById(id);
		if (validClient.isPresent()) {
			Client c = validClient.get();
			return c;
		} else return null;
	}
	
	
	public Client login(String email, String password) throws InvalidLoginException {
		if (null!=getClientByEmail(email)) {
				Client c = getClientByEmail(email);
				String encryptedPassword = DigestUtils.sha1Hex(password);
				if (c.getClientPassword().equals(encryptedPassword)) {
					return c;
				} else {
					throw new InvalidLoginException();
				}
		}
		throw new InvalidLoginException();
	}
	
	
	
	
}
