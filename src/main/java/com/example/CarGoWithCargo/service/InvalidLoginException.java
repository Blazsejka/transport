package com.example.CarGoWithCargo.service;

import com.example.CarGoWithCargo.viewmodel.LoginFormData;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString

public class InvalidLoginException extends Exception {
	public InvalidLoginException() {
		super("Wrong email or password!");
	}
	
		
}
