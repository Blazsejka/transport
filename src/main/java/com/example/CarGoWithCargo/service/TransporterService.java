package com.example.CarGoWithCargo.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.CarGoWithCargo.dao.TransporterDao;
import com.example.CarGoWithCargo.model.Client;
import com.example.CarGoWithCargo.model.Transporter;

@Service
public class TransporterService {
	
	@Autowired
	private TransporterDao transporterDao;
	
	
	public List<Transporter> getAllTransporter() {
		return transporterDao.findAll();
	}
	
	
	public void saveTransporter(Transporter t) {
		if (!(t.equals(null)))
		transporterDao.saveAndFlush(t);
	}
	
	
	public void saveFullTransporter(String name, String place, LocalDate date, String email, String phonenumber, String password, String company, String tax) {
		Transporter c = new Transporter();
		if (null!=name) {
			c.setClientName(name);
		}
		if (null!=place) {
			c.setClientPlaceOfBirth(place);
		}
		if (null!=date) {
			c.setClientDateOfBirth(date);
		}
		if (null!=email) {
			c.setClientEmail(email);
		}
		if (null!=phonenumber) {
			c.setClientPhoneNumber(phonenumber);
		}
		if (null!=password) {
			String encryptedPassword = DigestUtils.sha1Hex(password);
			c.setClientPassword(encryptedPassword);
		}
		if (null!=company) {
			c.setTransporterCompanyName(company);
		}
		if (null!=tax) {
			c.setTransporterTaxNumber(tax);
		}
		if (null!=c.getClientName() && null!=c.getClientDateOfBirth() && null!=c.getClientEmail() && null!=c.getClientPassword() && null!=c.getClientPhoneNumber() && null!=c.getClientPlaceOfBirth() && null!=c.getTransporterCompanyName() && null!=c.getTransporterTaxNumber()) {
			transporterDao.saveAndFlush(c);
		}
	}
	
	
	public Transporter getTransporterByName(String name) {
		Optional<Transporter> validTransporter = transporterDao.findByClientName(name);
		if (validTransporter.isPresent()) {
			Transporter c = validTransporter.get();
			return c;
		} else return null;
	}
	
	
	public Transporter getTransporterByEmail(String email) {
		Optional<Transporter> validTransporter = transporterDao.findByClientEmail(email);
		if (validTransporter.isPresent()) {
			Transporter c = validTransporter.get();
			return c;
		} else return null;
	}
	
	
	public Transporter getTransporterById(Long id) {
		Optional<Transporter> validTransporter = transporterDao.findById(id);
		if (validTransporter.isPresent()) {
			Transporter c = validTransporter.get();
			return c;
		} else return null;
	}
	
	
	public Transporter login(String email, String password) throws InvalidLoginException {
		if (null!=getTransporterByEmail(email)) {
			try {
				Transporter c = getTransporterByEmail(email);
				String encryptedPassword = DigestUtils.sha1Hex(password);
				if (c.getClientPassword().equals(encryptedPassword)) {
					return c;
				} else {
					throw new InvalidLoginException();
				}
			} finally {
				
			}
		} else {
			throw new InvalidLoginException();
		}
	}
	
	
	
	
	
	
	
}
