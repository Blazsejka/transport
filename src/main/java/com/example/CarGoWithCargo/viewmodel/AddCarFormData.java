package com.example.CarGoWithCargo.viewmodel;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class AddCarFormData {
	
	@Size(min=1, max=32, message="Car type must be between 1 and 32 characters")
	@NotNull(message="Please enter a valid car type")
	private String carType;
	
	@Size(min=1, max=32, message="Manufacturing year must be between 1 and 32 characters")
	@NotNull(message="Please enter a valid manufacturing year")
	private String carManufacturingYear;
	
	@Size(min=1, max=32, message="Car brand must be between 1 and 32 characters")
	@NotNull(message="Please enter a valid car brand")
	private String carBrand;
	
	@Size(min=1, max=32, message="Car model must be between 1 and 32 characters")
	@NotNull(message="Please enter a valid car model")
	private String carModel;
	
	@NotNull(message="Please enter a valid car total weight")
	@Min(value=3500)
	@Max(value=350000)
	private Integer carTotalWeight;
	
	@NotNull(message="Please enter a valid number of seats")
	@Min(value=1)
	@Max(value=150)
	private Integer carSeats;
	
	@Size(min=1, max=32, message="Plate number must be between 1 and 32 characters")
	@NotNull(message="Please enter a valid car model")
	private String carPlate;
	
	
}
