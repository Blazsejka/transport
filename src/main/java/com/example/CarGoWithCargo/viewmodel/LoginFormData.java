package com.example.CarGoWithCargo.viewmodel;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class LoginFormData {
	
	@Email
	@Size(min=1, max=32, message="User email must be between 1 and 32 characters")
	@NotNull(message="Please enter an email address")
	private String email;
	
	@Size(min=1, max=32, message="First name must be between 1 and 32 characters")
	@NotNull(message="Please enter a password")
	private String password;
	

}
