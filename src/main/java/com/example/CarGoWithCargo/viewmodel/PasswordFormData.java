package com.example.CarGoWithCargo.viewmodel;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class PasswordFormData {
	
	@Size(min=1, max=32, message="Password must be between 1 and 32 characters")
	@NotNull(message="Please enter a password")
	private String oldPassword;
	
	@Size(min=1, max=32, message="Password must be between 1 and 32 characters")
	@NotNull(message="Please enter a password")
	private String newPassword1;
	
	@Size(min=1, max=32, message="Password must be between 1 and 32 characters")
	@NotNull(message="Please enter a password")
	private String newPassword2;
	
	
	
}
