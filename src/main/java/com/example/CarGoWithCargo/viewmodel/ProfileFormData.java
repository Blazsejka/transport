package com.example.CarGoWithCargo.viewmodel;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class ProfileFormData {
	
	@Email
	@Size(min=1, max=32, message="User email must be between 1 and 32 characters")
	@NotNull(message="Please enter an email address")
	private String email;
	
	private String password1;
	
	private String password2;
	
	@Size(min=1, max=32, message="Place of birth must be between 1 and 32 characters")
	@NotNull(message="Please enter a place of birth")
	private String placeOfBirth;
	
	@Size(min=1, max=32, message="Date of birth must be between 1 and 32 characters")
	@NotNull(message="Please enter a date of birth")
	private String dateOfBirth;
	
	@Size(min=1, max=32, message="Phone number must be between 1 and 32 characters")
	@NotNull(message="Please enter a phone number")
	private String phoneNumber;
	
	@Size(min=1, max=32, message="Name must be between 1 and 32 characters")
	@NotNull(message="Please enter a name")
	private String name;
	
	private String companyName;
	
	private String taxNumber;
	
}
